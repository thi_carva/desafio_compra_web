# desafio_compra_web

Realizar uma compra em um site com sucesso

# **Teste** 

1.  Acessar o site: www.automationpractice.com 

2.  Escolher qualquer produto da loja e adicionar ao carrinho 

3.  Seguir para o checkout e verificar se o produto está no carrinho

4.  Realizar o cadastro do cliente e selecionar um método de pagamento

5.  Validar a compra e verificar se a mesma foi efetuada com sucesso


**Requisitos:** 

Utilizar a linguagem Ruby para codificação;

Realizar as interações do usuário utilizando o Capybara;

Escrever os cenários em gherkin;

Utilizar as boas práticas da automação;

**Observação**

Não existe automação certa ou errada, existem várias formas de se realizar uma mesma tarefa.

O objetivo deste teste é verificar somente o nível de conhecimento sobre as ferramentas e estruturação do projeto.
Após realizar os testes, subir para um repositório git de sua preferência e encaminhar o link para o e-mail:

[marina.jferreira@hsl.org.br](url)

